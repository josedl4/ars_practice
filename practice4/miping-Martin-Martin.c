// Practica tema 8, Martin Martin Jose Luis

#include <sys/types.h>
#include <sys/socket.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <netinet/in.h>
#include <netdb.h>
#include <string.h>
#include <arpa/inet.h>
#include <fcntl.h>

#include "ip-icmp-ping.h"

#define TRUE            1
#define FALSE           0

char msg[REQ_DATASIZE] = "Hello World";

static const char *msg_type_response[] = {
    "Echo reply: Echo reply",
    "unassigned",
    "unassigned",
    "Destination Unreachable",
    "Source Quench: deprecated",
    "Redirect Message",
    "deprecated",
    "unassigned",
    "Echo Request: Echo request",
    "Router Advertisement: Router Advertisement",
    "Router Solicitation: Router discovery/selection/solicitation",
    "Time Exceeded",
    "Parameter Problem: Bad IP header",
    "Timestamp: Timestamp",
    "Timestamp Reply: Timestamp Reply"
};

static const char *msg_type_response3[] = {
    "Destination network unreachable",              //3,0
    "Destination host unreachable",                 //3,1
    "Destination protocol unreachable",             //3,2
    "Destination port unreachable",                 //3,3
    "Fragmentation required, and DF flag set",      //3,4
    "Source route failed",                          //3,5
    "Destination network unknown",                  //3,6
    "Destination host unknown",                     //3,7
    "Source host isolated",                         //3,8
    "Network administratively prohibited",          //3,9
    "Host administratively prohibited",             //3,10
    "Network unreachable for ToS",                  //3,11
    "Host unreachable for ToS",                     //3,12
    "Communication administratively prohibited",    //3,13
    "Host Precedence Violation",                    //3,14
    "Precedence cutoff in effect"                   //3,15
};

static const char *msg_type_response5[] = {
    "Redirect Datagram for the Network",            //5,0
    "Redirect Datagram for the Host",               //5,1
    "Redirect Datagram for the ToS & network",      //5,2
    "Redirect Datagram for the ToS & host"          //5,3
};

static const char *msg_type_response11[] = {
    "TTL expired in transit",                       //11,0
    "Fragment reassembly time exceeded"             //11,1
};

static const char *msg_type_response12[] = {
    "Pointer indicates the error",
    "Missing a required option",
    "Bad length"
};

// Variable global para indicar si se encuentra en modo verbose
int verbose = FALSE;

/**
 * @brief Manejador de respuestas
 * 
 * @param type Tipo de respuesta
 * @param code Código de respuesta
 */
void responseHandler(uint8_t type, uint8_t code){
    switch(type){
        case 3:
            printf("Descripción de la respuesta: %s: %s (type %d, code %d)\n",
                msg_type_response[type], msg_type_response3[code], type, code);
            break;
        case 5:
            printf("Descripción de la respuesta: %s: %s (type %d, code %d)\n",
                msg_type_response[type], msg_type_response5[code], type, code);
            break;
        case 11:
            printf("Descripción de la respuesta: %s: %s (type %d, code %d)\n",
                msg_type_response[type], msg_type_response11[code], type, code);
            break;
        case 12:
            printf("Descripción de la respuesta: %s: %s (type %d, code %d)\n",
                msg_type_response[type], msg_type_response12[code], type, code);
            break;
        case 0: case 1:
        case 2: case 4:
        case 6: case 7:
        case 8: case 9:
        case 10: case 13:
        case 14:
            printf("Descripción de la respuesta: %s (type %d, code %d)\n",
                msg_type_response[type], type, code);
            break;
        default:
            printf("Unknown code  (type %d, code %d)\n", type, code);
    }
    
}

/**
 * @brief Función para calcular el checksum a partir de un buffer de bytes
 * 
 * @param array Buffer de bytes a calcular el checksum
 * @param numBytes Longitud del buffer
 * @return uint16_t checksum
 */
uint16_t calculateChecksum(uint16_t *array, int numBytes){
    int numShorts = numBytes / 2;
    uint32_t collector = 0;
    uint16_t *position = array;
    int i;

    for(i = 0; i < numShorts; i ++) {
        collector += (uint32_t) *position;
        position ++;
    }

    collector = (collector >> 16) + (collector & 0x0000ffff);
    collector = (collector >> 16) + (collector & 0x0000ffff);

    collector = ~collector;
    return (uint16_t)collector;
}

/**
 * Función principal
 */
int main (int argc, char** argv) {
    // Comprobamos los argumentos recibidos
    if(argc == 3 && strcmp(argv[2], "-v")==0){
        verbose = TRUE;
    } else if(argc != 2){
        printf("Numero de parametros incorrecto\n");
        exit(EXIT_FAILURE);
    }
    
    // Estructura con información del socket del cliente
    struct sockaddr_in addr_cli; 
    
    // Estructura con información del socket del servidor
    struct sockaddr_in addr_ser;
    
    // Identificador de socket
    int sock, n = 0;

    // socklen_t del servidor y cliente, con los tamaños de la estructura sockaddr_in
    socklen_t server_len, client_len;

    //Creamos el socket RAW
    sock = socket(AF_INET, SOCK_RAW, IPPROTO_ICMP); 
    if (sock<0) {perror("socket()"); exit(EXIT_FAILURE);}

    // Establecemos los valores de la estructura sockaddr_in para realizar el bind
    addr_cli.sin_family = AF_INET;
    addr_cli.sin_port = 0;
    addr_cli.sin_addr.s_addr = INADDR_ANY;

    // Guardamos el valor para la estructura socklen_t del cliente
    client_len = sizeof(addr_cli);

    // Realizamos el bind del socket creado anteriormente
    if( bind(sock, (struct sockaddr *) &addr_cli, client_len) != 0) {
        perror("bind()");
        exit(EXIT_FAILURE);
    }

    // Establecemos los valores del socket del servidor
    addr_ser.sin_family = AF_INET;
    addr_ser.sin_port = 0;

    // Añadimos la dirección IP del servidor
    if (inet_aton(argv[1], &addr_ser.sin_addr)==0) {
        perror("inet_aton()");
        exit(EXIT_FAILURE);
    }

    // Guardamos el valor para la estructura socklen_t del servidor
    server_len = sizeof(addr_ser);

    // Creamos la estructura ECHORequest
    ECHORequest echoRequest;
    memset(&echoRequest, 0, sizeof(ECHORequest));
    
    // Inicializamos sus valores para realizar el ping
    echoRequest.icmpHeader.Type = 8;
    echoRequest.icmpHeader.Code = 0;
    echoRequest.icmpHeader.Checksum = 0;
    
    if(verbose) {
        printf("-> Generando cabecera ICMP.\n");
        printf("-> Type: %d\n", echoRequest.icmpHeader.Type);
        printf("-> Code: %d\n", echoRequest.icmpHeader.Code);
    }
    
    echoRequest.ID = (unsigned short) getpid();
    echoRequest.SeqNumber = 0;
    strcpy(echoRequest.payload, msg);
    
    if(verbose) {
        printf("-> Identifier (pid): %d\n", echoRequest.ID);
        printf("-> Seq. Number: %d\n", echoRequest.SeqNumber);
        printf("-> Cadena a enviar: %s\n", echoRequest.payload);
    }

    // Calculamos el checksum
    uint16_t checksum = calculateChecksum((uint16_t *) &echoRequest, 
        sizeof(ECHORequest));
    
    echoRequest.icmpHeader.Checksum = checksum;

    if(verbose) {
        printf("-> Checksum: 0x%02x\n", checksum);
        printf("-> Tamaño total del paquete ICMP %lu\n",
            sizeof(ECHORequest));
    } 

    // Enviamos la request
    n = sendto(sock, &echoRequest, sizeof(ECHORequest), 0, 
        (struct sockaddr *)&addr_ser, server_len);
    if ( n < 0) {
        perror("sendto()");
        exit(EXIT_FAILURE);
    }
    printf("Paquete ICMP enviado a %s\n", argv[1]);
    
    // Preparamos la recepción del mensage
    ECHOResponse echoResponse;
    memset(&echoResponse, 0, sizeof(ECHOResponse));

    // Recibimos la response del ping realizado
    n = recvfrom(sock, &echoResponse, sizeof(ECHOResponse), 0, 
        (struct sockaddr *)&addr_ser, &client_len);
    if(n < 0) {
        perror("recvfrom()");
        exit(EXIT_FAILURE);
    }
    printf("Respuesta recibida desde %s\n", inet_ntoa(addr_ser.sin_addr));
    
    // Quitamos la cabecera IP para comprobar el checksum recibido
    if(verbose) printf("-> Tamaño de la respuesta: %d\n", n);

    checksum = calculateChecksum((uint16_t *) &echoResponse.icmpHeader, sizeof(ECHORequest));
    if(checksum != 0) {
        perror("Checksum response not correct");
        exit(EXIT_FAILURE);
    }
    
    // Informamos de los datos obtenidos
    if(verbose) {
        printf("-> Checksum correcto\n");
        printf("-> Cadena recibida: %s\n", echoResponse.payload);
        printf("-> Identifier (pid): %d\n", echoResponse.ID);
        printf("-> TTL: %d\n", echoResponse.ipHeader.TTL);
    }

    responseHandler((uint8_t)echoResponse.icmpHeader.Type, (uint8_t)echoResponse.icmpHeader.Code);

    return EXIT_SUCCESS;
}