// Practica tema 5, Martin Martin Jose Luis

#include <sys/types.h>
#include <sys/socket.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <netinet/in.h>
#include <netdb.h>
#include <string.h>
#include <arpa/inet.h>

#define SIZEBUF 512 // Tamaño del buffer

int main ( int argc, char *argv[] ) {

    //Estructura servent para almacenar información asociada a un servicio
    struct servent *serv; 

    // Estructura con información del socket del cliente
    struct sockaddr_in addr_cli; 
    
    // Estructura con información del socket del servidor
    struct sockaddr_in addr_ser; 
     
    // Puerto pasado como argumento del programa
    int port_in;

    // Puerto transformado para realizar la petición
    int port;
    
    // Identificador de socket
    int sock; 
    
    // Buffer para recibir el mensaje del servidor
    char buf[SIZEBUF]; 
    
    // socklen_t del servidor y cliente, con los tamaños de la estructura sockaddr_in
    socklen_t server_len, client_len; 

    /**
     * Tratamos los argumentos introducidos y obtenemos el
     * puerto destino de nuestra petición
     */
    switch (argc) {
        case 2:
            // Obtenemos información sobre el servicio daytime
            serv = getservbyname("daytime", "udp");
            if (!serv)
                printf("Unknown application DAYTIME\n");
            else{
                // Guardamos el puerto empleado para daytime por defecto
                port = serv->s_port;
            } 
            break;

        case 4:
            //Obtenemos el código ascii de la letra pasada como argumento
            if(getopt(argc, argv, "p:") != 'p') exit (EXIT_FAILURE); 
            //Comprobamos que sea un parametro permitido
            
            // Obtenemos el puerto pasado como argumento
            port_in = atoi(optarg);
            
            // Host to network short, convertimos numero de puerto para ser enviado por la red
            port = htons(port_in);
            
            break;
        default:
            printf("Input is not correct\n");
            exit (EXIT_FAILURE);
    }

    //Creamos el socket UDP
    sock = socket(AF_INET, SOCK_DGRAM, 0); 
    if (sock<0) {perror("socket()"); exit(EXIT_FAILURE);}
    
    // Establecemos los valores de la estructura sockaddr_in para realizar el bind
    addr_cli.sin_family = AF_INET;
    addr_cli.sin_port = 0;
    addr_cli.sin_addr.s_addr = INADDR_ANY;

    // Guardamos el valor para la estructura socklen_t del cliente
    client_len = sizeof(addr_cli);

    // Realizamos el bind del socket creado anteriormente
    if( bind(sock, (struct sockaddr *) &addr_cli, client_len) != 0) {
        perror("bind()");
        exit(EXIT_FAILURE);
    }

    // Establecemos los valores del socket del servidor
    addr_ser.sin_family = AF_INET;
    addr_ser.sin_port = port;

    // Añadimos la dirección IP del servidor
    if (inet_aton(argv[1], &addr_ser.sin_addr)==0) {
        perror("inet_aton()");
        exit(EXIT_FAILURE);
    }

    // Guardamos el valor para la estructura socklen_t del servidor
    server_len = sizeof(addr_ser);

    // Mandamos al servidor un datagrama sin contenido para que nos responda con la fecha y hora
    if (sendto(sock, NULL, 0, 0, (struct sockaddr *)&addr_ser, server_len) == -1) {
        perror("sendto()");
        exit(EXIT_FAILURE);
    }

    // Esperamos la respuesta del servidor
    if(recvfrom(sock, buf, SIZEBUF, 0, (struct sockaddr *)&addr_ser, &server_len) == -1) {
        perror("recvfrom()");
        exit(EXIT_FAILURE);
    }
    
    // Mostramos el resultado obtenido
    printf("%s", buf);

    // Por ultimo cerramos el socket al finalizar el programa
    if( close(sock) != 0 ) {
        perror("close()");
        exit(EXIT_FAILURE);
    }

    return EXIT_SUCCESS;
}
