// Practica tema 5, Martin Martin Jose Luis

#include <sys/types.h>
#include <sys/socket.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <netinet/in.h>
#include <netdb.h>
#include <string.h>
#include <arpa/inet.h>

#define SIZEBUF 45

/**
 * Función que genera el contenido
 * en el buffer con el dia y hora
 */
void get_daytime(char* buff){

    FILE *fich;
    char name[15];
    char time[30];

    // Obtenemos el nombre de Host
    if(gethostname(name, sizeof(name)) == -1) {
        perror("gethostname()");
        exit(EXIT_FAILURE);
    }
    
    // Obtenemos la fecha y hora del sistema
    system("date > /tmp/tt.txt");
    fich = fopen("/tmp/tt.txt","r");
    
    if ( fgets(time, SIZEBUF, fich) == NULL ) {
        printf("Error en system(), en fopen(), o en fgets()\n");
        exit(EXIT_FAILURE);
    }

    // Concatenamos las distintas cadenas en el buffer
    strcpy(buff, name);
    strcat(buff, ": ");
    strcat(buff, time);
}


int main ( int argc, char *argv[] ) {

    //Estructura servent para almacenar información asociada a un servicio
    struct servent *serv; 

    // Estructura con información del socket del cliente
    struct sockaddr_in addr_cli;
    
    // Estructura con información del socket del servidor
    struct sockaddr_in addr_ser;

    // Puerto pasado como argumento del programa
    int port_in;

    // Puerto transformado para realizar la petición
    int port;
    
    // Identificador de socket
    int sock;

    // socklen_t del servidor y cliente, con los tamaños de la estructura sockaddr_in
    socklen_t server_len, client_len;
    
    // Buffer para recibir el mensaje del servidor
    char buf[SIZEBUF];

    /**
     * Tratamos los argumentos introducidos y obtenemos el
     * puerto destino de nuestra petición
     */
    switch (argc) {
        case 1:            
            // Obtenemos información sobre el servicio daytime
            serv = getservbyname("daytime", "udp");
            if (!serv)
                printf("unknown application DAYTIME\n");
            else{
                // Guardamos el puerto empleado para daytime por defecto
                port = serv->s_port;
            } 
            break;

        case 3:
            //Obtenemos el codigo ascii de la letra pasada como argumento
            if(getopt(argc, argv, "p:") != 'p') exit (EXIT_FAILURE); //Comprobamos que sea un parametro permitido
            
            // Obtenemos el puerto pasado como argumento
            port_in = atoi(optarg);
            
            // Host to network short, convertimos numero de puerto para ser enviado por la red
            port = htons(port_in);
            
            break;
        default:
            printf("Input is not correct\n");
            exit (EXIT_FAILURE);
    }

    // Creamos el socket para recibir peticiones
    sock = socket(AF_INET, SOCK_DGRAM, 0);
    if (sock<0) {perror("socket()"); exit(EXIT_FAILURE);}
    
    // Establecemos los valores de la estructura sockaddr_in para realizar el bind
    addr_ser.sin_family = AF_INET;
    addr_ser.sin_port = port;
    addr_ser.sin_addr.s_addr = INADDR_ANY;

    // Guardamos el valor para la estructura socklen_t del servidor
    server_len = sizeof(addr_ser);

    // En lazamos el socket con el puerto obtenido
    if( bind(sock, (struct sockaddr *) &addr_ser, server_len) != 0) {
        perror("bind()");
        exit(EXIT_FAILURE);
    }

    // Guardamos el valor para la estructura socklen_t del cliente
    client_len = sizeof(addr_cli);
    
    // Bucle infinito para recibir los mensajes de los clientes
    while(1) {
        // Se mantiene en espera activa hasta recibir un datagrama
        if (recvfrom(sock, NULL, 0, 0, (struct sockaddr *)&addr_cli, &client_len)==-1){
            perror("recvfrom()");
            exit(EXIT_FAILURE);
        }
        
        // Informa de quien se ha recibido la petición
        printf("Peticion recibida de: %s \n ", inet_ntoa(addr_cli.sin_addr));        
        
        // Se genera en el buffer la estructura de la respuesta
        get_daytime(buf);

        // Responde al cliente con un buffer que contiene la inforamción sobre el dia y hora
        if (sendto(sock, buf, SIZEBUF, 0, (struct sockaddr *)&addr_cli, client_len)==-1){
            perror("sendto()");
            exit(EXIT_FAILURE);
        }
    }

    
    return EXIT_SUCCESS;
}
