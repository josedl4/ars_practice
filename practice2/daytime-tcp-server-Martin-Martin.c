// Practica tema 6, Martin Martin Jose Luis

#include <sys/types.h>
#include <sys/socket.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <netinet/in.h>
#include <netdb.h>
#include <string.h>
#include <arpa/inet.h>
#include <signal.h>

#define SIZEBUF 45
#define LISTEN 8

// Identificador de socket del padre
int sock = -1;

// Conexión creada del socket para los hijos
int connection = -1;

// PID hijo
pid_t child_pid;

/**
 * Función que genera el contenido
 * en el buffer con el dia y hora
 */
void get_daytime(char* buff){

    FILE *fich;
    char name[15];
    char time[30];

    // Obtenemos el nombre de Host
    if(gethostname(name, sizeof(name)) == -1) {
        perror("gethostname()");
        exit(EXIT_FAILURE);
    }
    
    // Obtenemos la fecha y hora del sistema
    system("date > /tmp/tt.txt");
    fich = fopen("/tmp/tt.txt","r");
    
    if ( fgets(time, SIZEBUF, fich) == NULL ) {
        printf("Error en system(), en fopen(), o en fgets()\n");
        exit(EXIT_FAILURE);
    }

    // Concatenamos las distintas cadenas en el buffer
    strcpy(buff, name);
    strcat(buff, ": ");
    strcat(buff, time);
}


/**
 * Función para desconectar el socket recibido
 */
void disconnectSocket(int socket) {
    
    // Comprobamos que sea un socket con ID valido
    if(socket >= 0) {

        // Realizamos un recv previo a que cerremos la conexión para evitar el bloqueo de puertos
        if(recv(connection, NULL, 0, 0) > 0) {
            perror("Failure to close the connection");
            exit(EXIT_FAILURE);
        }

        // Notificamos de que queremos cerrar la conexión
        if( shutdown(socket, SHUT_RDWR) != 0 ) {
            perror("shutdown()");
            exit(EXIT_FAILURE);
        } 

        // Comprobamos que se ha cerrado la conexión
        if(recv(socket, NULL, 0, 0) > 0) {
            perror("Failure to close the connection");
            exit(EXIT_FAILURE);
        }

        // Por ultimo cerramos el socket al finalizar el programa
        if( close(socket) != 0 ) {
            perror("close()");
            exit(EXIT_FAILURE);
        }

        printf("Closed socket with ID: %d\n", socket);
    }
    
}


/**
 * Manejador de señales recibidas
 */
void signalHandler(int s) {
    
    // Si se trata del proceso padre cerramos el socket principal
    if(child_pid != 0){
        disconnectSocket(sock);
    // Si se trata del hijo cerramos el socket connection empleado para dar servicio
    }else {
        disconnectSocket(connection);
    }
        
    exit(EXIT_FAILURE);
}


/**
 * Función main
 */
int main(int argc, char * argv[]){
    signal(SIGINT, signalHandler);

    //Estructura servent para almacenar información asociada a un servicio
    struct servent *serv; 
    
    // Estructura con información del socket del cliente
    struct sockaddr_in addr_cli;
    
    // Estructura con información del socket del servidor
    struct sockaddr_in addr_ser;

    // Puerto pasado como argumento del programa
    int port_in;

    // Puerto transformado para realizar la petición
    int port;
    
    // socklen_t del servidor y cliente, con los tamaños de la estructura sockaddr_in
    socklen_t server_len, client_len;
    
    // Buffer para recibir el mensaje del servidor
    char buf[SIZEBUF];

    /**
     * Tratamos los argumentos introducidos y obtenemos el
     * puerto destino de nuestra petición
     */
    switch (argc) {
        case 1:            
            // Obtenemos información sobre el servicio daytime
            serv = getservbyname("daytime", "tcp");
            if (!serv)
                printf("unknown application DAYTIME\n");
            else{
                // Guardamos el puerto empleado para daytime por defecto
                port = serv->s_port;
            } 
            break;

        case 3:
            //Obtenemos el codigo ascii de la letra pasada como argumento
            if(getopt(argc, argv, "p:") != 'p') exit (EXIT_FAILURE); //Comprobamos que sea un parametro permitido
            
            // Obtenemos el puerto pasado como argumento
            port_in = atoi(optarg);
            
            // Host to network short, convertimos numero de puerto para ser enviado por la red
            port = htons(port_in);
            
            break;
        default:
            printf("Input is not correct\n");
            exit (EXIT_FAILURE);
    }

    //Creamos el socket TCP
    sock = socket(AF_INET, SOCK_STREAM, 0); 
    if (sock<0) {perror("socket()"); exit(EXIT_FAILURE);}
    
    printf("Create socket with ID: %d\n", sock);

    // Establecemos los valores de la estructura sockaddr_in para realizar el bind
    addr_ser.sin_family = AF_INET;
    addr_ser.sin_port = port;
    addr_ser.sin_addr.s_addr = INADDR_ANY;

    // Guardamos el valor para la estructura socklen_t del servidor
    server_len = sizeof(addr_ser);

    // En lazamos el socket con el puerto obtenido
    if( bind(sock, (struct sockaddr *) &addr_ser, server_len) != 0) {
        perror("bind()");
        exit(EXIT_FAILURE);
    }

    // Establece el socket en modo escucha
    if(listen(sock, LISTEN) == -1) {
        perror("listen()");
        exit(EXIT_FAILURE);
    }

    // Bucle infinito para dar servicio a las peticiones
    while(1) {
        client_len = sizeof(addr_cli);

        // Crea un nuevo socket para atender la conexión con un cliente
        if((connection = accept(sock, (struct sockaddr *) &addr_cli, &client_len)) == -1){
            perror("accept()");
            exit(EXIT_FAILURE);
        }

        printf("Open socket connection with ID: %d\n", connection);

        // Crea un proceso hijo para dar servicio al cliente sin generar bloqueo
        if((child_pid = fork()) == 0) {

            // Obtenemos la fecha y hora
            get_daytime(buf);
            
            // Realizamos el envió
            if(send(connection, buf, SIZEBUF, 0) == -1){
                perror("send()");
                EXIT_FAILURE;
            }

            // Liberamos recursos
            disconnectSocket(connection);
            return EXIT_SUCCESS;
        }

    }

    return EXIT_SUCCESS;
}