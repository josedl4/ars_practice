// Practica tema 7, Martin Martin Jose Luis

#include <sys/types.h>
#include <sys/socket.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <netinet/in.h>
#include <netdb.h>
#include <string.h>
#include <arpa/inet.h>
#include <fcntl.h>

/**%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 * %                 Defines               % 
 * %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%*/
#define RRQ    1
#define WRQ    2
#define DATA   3
#define ACK	   4
#define ERROR  5

#define INIT_DGRAM      0
#define DATA_SIZE       512
#define FILENAME        100
#define MODE            100
#define ERR_SIZE        100

#define TRUE            1
#define FALSE           0

/**%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 * %                 Structs               % 
 * %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%*/
typedef struct {
	uint16_t op;
	char filename[FILENAME];
	char eos_0;
	char mode[MODE];
	char eos_1;
} Request;

typedef struct {
	uint16_t op;
	uint16_t n_block;
	char data[DATA_SIZE];
} Data;

typedef struct {
	uint16_t op;
	uint16_t n_block;
} Ack;

typedef struct {
    uint16_t op;
    uint16_t errcode;
    char errstring[ERR_SIZE];
    char eos_0;
} Error;
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

// Array con los posibles tipos de errores
static const char *error_msg[] = {
	"Error no definido",
	"Fichero no encontrado",
	"Violación de acceso",
	"Espacio insuficiente",
	"Operación ilegal",
	"Identificador desconocido",
	"Fichero ya existe",
	"Usuario desconocido"
};

// Variable global para indicar si se encuentra en modo verbose
int verbose = FALSE;

/**
 * @brief Manejador de errores
 * 
 * @param error Estructura de error
 */
void errorHandler(Error *error){
    // Al recibir un error mostramos el tipo, código y errstring
    printf("ERROR CODE %d (%s) - ERRSTRING: %s\n", ntohs(error->errcode),
        error_msg[ntohs(error->errcode)], error->errstring);
}

/**
 * @brief Comprueba la existencia de dicho fichero en el directorio
 * 
 * @param fileName Nombre del fichero
 * @return int Valor de verdad en función de si existe el fichero
 */
int fileExist(char *fileName) {
    FILE *file;
    file = fopen(fileName, "r");
    if (file){
       return TRUE;
    }else{
       return FALSE;
    }
}

/**
 * @brief Completamos una estructura de tipo request
 * 
 * @param request Estructura request para enviar al servidor
 * @param op Código de Operación
 * @param filename Nombre del fichero con el que vamos a trabajar
 * @param mode Modo de funcionamiento
 */
void createRequest(Request *request, uint16_t op,
    char *filename, char *mode) {
        memset(request, 0, sizeof(Request));
        request->op = htons(op);
        strcpy(request->filename, filename);
        request->eos_0 = '\0';
        strcpy(request->mode, "octet");
        request->eos_1 = '\0';
}

/**
 * @brief Convierte una estructura request a un array de bytes para mandar
 * 
 * @param request Estructura request
 * @param buf Buffer con la estructura convertida
 */
void requestToCharArray(Request *request, char *buf) {
    char *position = buf;
    // Pasamos cada uno de los campos de la request a un buffer
    // y avanzamos tanto como ocupen cada uno de los valores
    *(uint16_t*)position = request->op;
    position += sizeof(request->op);
    
    strcpy(position, request->filename);
    position += strlen(request->filename);

    *position = request->eos_0;
    position += 1;

    strcpy(position, request->mode);
    position += strlen(request->mode);

    *position = request->eos_1;
    position += 1;
}

/**
 * @brief Escribe en el servidor
 * 
 * @param addr Dirección del servidor
 * @param filename Nombre del fichero
 */
void writeTFTP(char *addr, char *filename) {
    //Estructura servent para almacenar información asociada a un servicio
    struct servent *serv; 
    
    // Estructura con información del socket del cliente
    struct sockaddr_in addr_cli; 
    
    // Estructura con información del socket del servidor
    struct sockaddr_in addr_ser;

    // Puerto transformado para realizar la petición
    int port;
    
    // Identificador de socket
    int sock;

    // socklen_t del servidor y cliente, con los tamaños de la estructura sockaddr_in
    socklen_t server_len, client_len;

    // Obtenemos información sobre el servicio tftp
    serv = getservbyname("tftp", "udp");
    if (!serv)
        printf("Unknown application TFTP\n");
    else{
        // Guardamos el puerto empleado para daytime por defecto
        port = serv->s_port;
    } 

    //Creamos el socket UDP
    sock = socket(AF_INET, SOCK_DGRAM, 0); 
    if (sock<0) {perror("socket()"); exit(EXIT_FAILURE);}
    
    // Establecemos los valores de la estructura sockaddr_in para realizar el bind
    addr_cli.sin_family = AF_INET;
    addr_cli.sin_port = 0;
    addr_cli.sin_addr.s_addr = INADDR_ANY;

    // Guardamos el valor para la estructura socklen_t del cliente
    client_len = sizeof(addr_cli);

    // Realizamos el bind del socket creado anteriormente
    if( bind(sock, (struct sockaddr *) &addr_cli, client_len) != 0) {
        perror("bind()");
        exit(EXIT_FAILURE);
    }

    // Establecemos los valores del socket del servidor
    addr_ser.sin_family = AF_INET;
    addr_ser.sin_port = port;

    // Añadimos la dirección IP del servidor
    if (verbose) printf("Accediendo al servidor con IP: %s\n", addr);
    if (inet_aton(addr, &addr_ser.sin_addr)==0) {
        perror("inet_aton()");
        exit(EXIT_FAILURE);
    }

    // Guardamos el valor para la estructura socklen_t del servidor
    server_len = sizeof(addr_ser);

    // Creamos estructura Request
    Request *request = (Request *) malloc(sizeof(Request));
    if(!request){
        perror("malloc()");
        exit(EXIT_FAILURE);
    }
    createRequest(request, WRQ, filename, "octet");

    // Creamos un buffer para almacenar la request
    char *buf_request = (char*)malloc(sizeof(Request));
    if(!buf_request){
        perror("malloc()");
        exit(EXIT_FAILURE);
    }
    requestToCharArray(request, buf_request);
    
    //Realizamos el envió de la solicitud
    if (sendto(sock, buf_request, sizeof(Request), 0, (struct sockaddr *)&addr_ser, server_len) < 0) {
        perror("sendto()");
        exit(EXIT_FAILURE);
    }

    // Preparamos el envió del fichero
    int filesize, pos = 0, next_ack = FALSE, block = 0, n, last = FALSE;

    // Leemos el fichero
    int file = open(filename, O_RDONLY);
	if (file == -1) {
		perror("open()");
		exit(EXIT_FAILURE);
	}
	filesize = lseek(file, 0, SEEK_END);
    lseek(file, 0, SEEK_SET);

    // Creamos estructura Data
    Data *data_pkg = malloc(sizeof(Data));
    if(!data_pkg){
        perror("malloc()");
        exit(EXIT_FAILURE);
    }
    memset(data_pkg, 0, sizeof(Data));

    // Creamos estructura ACK
    Ack *ack = (Ack *) malloc(sizeof(Ack));
    if(!ack){
        perror("malloc()");
        exit(EXIT_FAILURE);
    }
    memset(ack, 0, sizeof(Ack));

    // Reservamos memoria para recibir la respuesta, del tamaño maximo posible
    char buf_response [sizeof(Error)];
    memset(buf_response, 0, sizeof(Error));

    // Obtenemos respuesta del servidor
    if(recvfrom(sock, buf_response, sizeof(Error), 0, (struct sockaddr *)&addr_ser, &client_len) < 0) {
        perror("recvfrom()");
        exit(EXIT_FAILURE);
    }

    // Comprobamos si se trata de un ACK o un mensaje de ERROR
    ack = (Ack *) buf_response;
    if(ntohs(ack->op) == ERROR) {
        Error *error = (Error *) buf_response;
        errorHandler(error);
        exit(EXIT_FAILURE);
    }

    // Comenzamos con el envió de lo distintos paquetes
    while(pos <= filesize) {
        memset(data_pkg, 0, sizeof(Data));
        memset(ack, 0, sizeof(Ack));

        // Leemos del fichero los bytes para cada paquete
        char buf_data [DATA_SIZE] = {0};
        n = read(file, &buf_data, DATA_SIZE);
        if(n == -1) {
            perror("read()");
            exit(EXIT_FAILURE);
        }

        // Preparamos el paquete Data
        pos += n;
        block++;
        if(verbose) printf("Bloque numero %d, tamaño %d bytes\n", block, n);
        if(n < DATA_SIZE) 
            last = TRUE;
        else
            last = FALSE;
        data_pkg->op = htons(DATA);
        data_pkg->n_block = htons(block);

        memcpy(data_pkg->data, buf_data, n);
        if(verbose) printf("\tEnviando bloque %d\n", block);

        // Enviamos los datos
        if (sendto(sock, data_pkg, sizeof(Data), 0, (struct sockaddr *)&addr_ser, server_len) < 0) {
            perror("sendto()");
            exit(EXIT_FAILURE);
        }

        // Nos mantenemos a la espera del ACK correspondiente
        while(!next_ack) {
            memset(ack, 0, sizeof(Ack));
            memset(buf_response, 0, sizeof(Error));
            
            // Recibimos datos del servidor
            n = recvfrom(sock, buf_response, sizeof(Error), 0, (struct sockaddr *)&addr_ser, &client_len);
            if(n < 0) {
                perror("recvfrom()");
                exit(EXIT_FAILURE);
            }

            // Comprobamos el tipo de datos recibidos
            ack = (Ack *) buf_response;
            
            if(ntohs(ack->op) == ERROR) {
                Error *error = (Error *) buf_response;
                errorHandler(error);
                exit(EXIT_FAILURE);
            } else {
                // Comprobamos que se trata del ACK correcto
                if(ntohs(ack->n_block) == block) {
                    next_ack = TRUE;
                    if(last) {
                        if(verbose) printf("\tRecibido ACK del ultimo paquete, numero %d\n", block);
                        break;
                    } else {
                        if(verbose) printf("\tRecibido ACK del paquete numero %d\n", block);
                        continue;
                    }
                } else if(ntohs(ack->n_block) < block) {
                    if(verbose) 
                        printf("Erro en el ACK recibido, se esperaba el %d y se recibió el %d\n",
                         block, ntohs(ack->n_block));
                    exit(EXIT_FAILURE);
                }
            }
        }

        next_ack = FALSE;
        if(last) break;
    }

}

/**
 * @brief Metodo encargado de leer datos del servidor
 * 
 * @param addr Dirección del servidor
 * @param filename Nombre del fichero de datos
 */
void readTFTP(char *addr, char *filename) {
    //Estructura servent para almacenar información asociada a un servicio
    struct servent *serv; 
    
    // Estructura con información del socket del cliente
    struct sockaddr_in addr_cli; 
    
    // Estructura con información del socket del servidor
    struct sockaddr_in addr_ser;

    // Puerto transformado para realizar la petición
    int port;
    
    // Identificador de socket
    int sock;

    // socklen_t del servidor y cliente, con los tamaños de la estructura sockaddr_in
    socklen_t server_len, client_len;

    // Obtenemos información sobre el servicio tftp
    serv = getservbyname("tftp", "udp");
    if (!serv)
        printf("Unknown application TFTP\n");
    else{
        // Guardamos el puerto empleado para daytime por defecto
        port = serv->s_port;
    } 

    //Creamos el socket UDP
    sock = socket(AF_INET, SOCK_DGRAM, 0); 
    if (sock<0) {perror("socket()"); exit(EXIT_FAILURE);}
    
    // Establecemos los valores de la estructura sockaddr_in para realizar el bind
    addr_cli.sin_family = AF_INET;
    addr_cli.sin_port = 0;
    addr_cli.sin_addr.s_addr = INADDR_ANY;

    // Guardamos el valor para la estructura socklen_t del cliente
    client_len = sizeof(addr_cli);

    // Realizamos el bind del socket creado anteriormente
    if( bind(sock, (struct sockaddr *) &addr_cli, client_len) != 0) {
        perror("bind()");
        exit(EXIT_FAILURE);
    }

    // Establecemos los valores del socket del servidor
    addr_ser.sin_family = AF_INET;
    addr_ser.sin_port = port;

    // Añadimos la dirección IP del servidor
    if (verbose) printf("Accediendo al servidor con IP: %s\n", addr);
    if (inet_aton(addr, &addr_ser.sin_addr)==0) {
        perror("inet_aton()");
        exit(EXIT_FAILURE);
    }

    // Guardamos el valor para la estructura socklen_t del servidor
    server_len = sizeof(addr_ser);

    // Creamos estructura Request
    Request *request = (Request *) malloc(sizeof(Request));
    if(!request){
        perror("malloc()");
        exit(EXIT_FAILURE);
    }
    createRequest(request, RRQ, filename, "octet");
    char *buf_request = (char*)malloc(sizeof(Request));
    if(!buf_request){
        perror("malloc()");
        exit(EXIT_FAILURE);
    }
    requestToCharArray(request, buf_request);

    if (sendto(sock, buf_request, sizeof(Request), 0, (struct sockaddr *)&addr_ser, server_len) < 0) {
        perror("sendto()");
        exit(EXIT_FAILURE);
    }

    // Preparamos la recepcion de datos
    int current_block = 0, next_block = 1;

    FILE *file = fopen(filename, "ab");

    // Creamos estructura Data
    Data *data_pkg = malloc(sizeof(Data));
    if(!data_pkg){
        perror("malloc()");
        exit(EXIT_FAILURE);
    }
    memset(data_pkg, 0, sizeof(Data));

    // Creamos estructura ACK
    Ack *ack = (Ack *) malloc(sizeof(Ack));
    if(!ack){
        perror("malloc()");
        exit(EXIT_FAILURE);
    }
    memset(ack, 0, sizeof(Ack));
    
    // Estamos a la espera de recibir paquetes y respondemos a cada paquete
    // con su correspondiente ACK
    while(TRUE) {    
        int len;
        // Recibimos datos
        if(recvfrom(sock, data_pkg, sizeof(Data), 0, (struct sockaddr *)&addr_ser, &client_len) < 0) {
            perror("recvfrom()");
            exit(EXIT_FAILURE);
        }

        // Comprobamos que no se trate de un error
        if(ntohs(data_pkg->op) == ERROR){
            Error *error = (Error *) data_pkg;
            errorHandler(error);
            remove(filename);
            exit(EXIT_FAILURE);
        }

        // Comprobamos que no se trate de un paquete ya recibido
        if (ntohs(data_pkg->n_block) < next_block) {
            if (verbose) printf("Recibido paquete duplicado numero %d\n", ntohs(data_pkg->n_block));
            ack->op = htons(ACK);
			ack->n_block = htons(current_block);
		} else {
            len = strlen(data_pkg->data);

            // Guardamos los datos recibidos
			char buf_data[DATA_SIZE+1];
            strcpy(buf_data, data_pkg->data);
            
            if (verbose) printf("Recibido paquete numero: %d, con un tamaño de %d bytes\n", ntohs(data_pkg->n_block), len);

            // Escribimos el fichero
            fwrite(buf_data, sizeof(char), len, file);

            // Preparamos el ACK
            current_block = next_block;
            next_block ++;
            ack->op = htons(ACK);
			ack->n_block = htons(current_block);
        }

        // Mandamos el ACK correspondiente
        if (verbose) printf("Mandamos ACK del bloque %d\n", current_block);
        if (sendto(sock, ack, sizeof(Ack), 0, (struct sockaddr *)&addr_ser, server_len) < 0) {
            perror("sendto()");
            exit(EXIT_FAILURE);
        }
        
        
        if(len < DATA_SIZE) break;
        memset(data_pkg, 0, sizeof(Data));
        memset(ack, 0, sizeof(Ack));
    }

    // Liberamos recursos
    free(ack);
    free(data_pkg);
    free(request);
    free(buf_request);
    close(sock);
}

/**
 * Función principal
 */
int main (int argc, char** argv) {
    int read = -1;
    char firstOption [2];
    
    // Nombre del fichero
    char *file = NULL;
    
    /**
     * Tratamos los argumentos introducidos
     */
    switch (argc) {
        case 4:
            strcpy(firstOption, argv[2]);
            if(strcmp(firstOption, "-r")==0) {
                read = 1;
                break;
            } else if(strcmp(firstOption, "-w")==0) {
                read = 0;
                break;
            } else {
                printf("Input is not correct\n");
                exit (EXIT_FAILURE);
            }
        case 5:
            strcpy(firstOption, argv[2]);
            if(strcmp(firstOption, "-r")==0) {
                read = 1;
            } else if(strcmp(firstOption, "-w")==0) {
                read = 0;
            } else {
                printf("Input is not correct\n");
                exit (EXIT_FAILURE);
            }

            strcpy(firstOption, argv[4]);
            if(strcmp(firstOption, "-v")==0) {
                verbose = TRUE;
            } else {
                printf("Input is not correct\n");
                exit (EXIT_FAILURE);
            }
            break;
        default:
            printf("Input is not correct\n");
            exit (EXIT_FAILURE);
    }

    //Reservamos memoria para el nombre del fichero y guardamos
    file = (char *)malloc(strlen(argv[3]));
    if(!file){
        perror("malloc()");
        exit(EXIT_FAILURE);
    }
    strcpy(file, argv[3]);

    // Según los argumentos introducidos realizamos la acción correspondiente  
    if(read == 1) {
        // Comprobamos que el fichero a leer no existe ya
        if(fileExist(file) == TRUE){
            if(verbose) printf("File already exist in this folder\n");
            exit(EXIT_FAILURE);
        } 
        readTFTP(argv[1], file);
    } else if(read == 0){
        // Comprobamos que el fichero a escribir si existe
        if(fileExist(file) == FALSE){
            if(verbose) printf("File not found\n");
            exit(EXIT_FAILURE);
        }
        writeTFTP(argv[1], file);
    } else {
        perror("Input is not correct");
        exit(EXIT_FAILURE);
    }

    return EXIT_SUCCESS;
}