# Practices of the Network and Services Architecture subject

## Practice 1: Daytime server and client using UDP
---
**Build**

Build client
```
gcc -Wall -o client.out daytime-udp-client-Martin-Martin.c
```
Build server
```
gcc -Wall -o server.out daytime-udp-server-Martin-Martin.c
```

**Run**

## Practice 2: Daytime server and client using TCP
---
**Build**

Build client
```
gcc -Wall -o client.out daytime-tcp-client-Martin-Martin.c
```
Build server
```
gcc -Wall -o server.out daytime-tcp-server-Martin-Martin.c
```

**Run**

## Practice 3: Client of a TFTP service using UDP
---

**Build**
```
gcc -Wall -o client.out tftp-Martin-Martin.c
```

**Run**
```
./client.out [Server IP] -[w|r] [Name of file] [-v]
```
*Flags:*
 * -v Activate verbose mode
 * -r Read file from server
 * -w Write file in server

## Practice 4: In Progress 